﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventureDemo
{
    class Item : Entity
    {
        public static List<Item> AllItems = new List<Item>();

        public Item(string name, string desc, int health = 1, int damage = 1)
        {
            Name = name;
            Description = desc;
            Health = health;
            Damage = damage;

            AllEntitys.Add(this);
        }

        //Item variables
        public bool Hidden = false;
        public bool CanTake = false;

        //Can be used to unlock item
        public Item Key = null;

        //If it's a potion or whatever it will do this much damage or health to the player
        public int HealthEffect = 0;

        //Default weight of the items, in case I want to limit how much the player
        //can take in their inventory
        public int Weight = 1;

        //Drops any items entity contained
        public void Death()
        {

        }

        //Tries to unlock item
        public void Unlock(Entity trykey)
        {
            if (trykey == Key)
            {
                //Allow players to take items
                TakeItems(trykey.Owner);
            }
        }

        public void TakeItems(Entity Taker)
        {

        }

        public static Item GetItem(string name)
        {
            name = name.ToLower().Replace(" ", "");
            foreach (Item i in AllItems)
            {
                if (i.Name.ToLower().Replace(" ", "") == name)
                {
                    return i;
                }
            }
            throw new Exception("Could not find item named: " + name);
        }

    }
}
