﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventureDemo
{
    class Domestic
    {
        public List<Item> GenericSeats = new List<Item>();
        public List<Item> GenericLighting = new List<Item>();

        public Domestic()
        {
            //generate our basic items...
            #region seats
            Item stool = new Item("Stool", "A wooden old stool. It seems rather sturdy.");
            Item chair = new Item("Chair", "A sturdy wooden chair.");
            Item bench = new Item("Bench", "A sturdy wooden bench made of strong oak planks.");
            Item rockingChair = new Item("Rocking Chair", "A wooden rocking chair with a blanket on it for padding. It looks quite comfortable.");
            Item blanket = new Item("Wool Blanket", "A funny smelling wool blanket.");
            rockingChair.Items.Add(blanket);
            GenericSeats.Add(stool);
            GenericSeats.Add(chair);
            GenericSeats.Add(bench);
            GenericSeats.Add(rockingChair);
            #endregion

            #region lighting
            Item lamp = new Item("Lamp", "A lamp half filled with oil.");
            Item candle = new Item("Candle", "A candle in a wooden candlestand.");
            Item torch = new Item("Torch", "A small torch which looks half-way burnt down.");
            Item candleLamp = new Item("Candle Lamp", "A candle sits inside a red-tinted lamp.");
            GenericLighting.Add(lamp);
            GenericLighting.Add(candle);
            GenericLighting.Add(torch);
            GenericLighting.Add(candleLamp);
            #endregion

        }

        public Entity GenerateOnePersonBedroom()
        {
            Random R = new Random(DateTime.Now.Second);
            //Create testing rooms
            Entity bedroom = new Entity("Small Bedroom", "A relatively small bedroom for one person.");
            Item bed = new Item("One person bed", "A one person bed made of wood.");

            Item nightstand = new Item("Nightstand", "A wooden nightstand with a drawer.");
            Item book = new Item("A Tale of Magic", "An old copy of a Tale of Magic, a book about adventurers who go on a magical journey to save a princess.");
            nightstand.Items.Add(book);

            bedroom.Items.Add(bed);
            bedroom.Items.Add(nightstand);
            //lighting
            bedroom.Items.Add(GenericLighting[R.Next(0, GenericLighting.Count)]);
            //seating
            for (int i = 0; i < R.Next(0, 3); i++)
            {
                bedroom.Items.Add(GenericSeats[R.Next(0, GenericSeats.Count)]);
            }
            return bedroom;
        }

    }
}
