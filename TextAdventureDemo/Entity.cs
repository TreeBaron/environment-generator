﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventureDemo
{
    class Entity
    {

        public static int IDCount = 0;

        //Used to keep track of and update all entities
        public static List<Entity> AllEntitys = new List<Entity>();
        //Used for dices rolls
        public static Random R = new Random(DateTime.Now.Second);

        public static int TurnNumber = 0;

        public Entity(string name, string desc, int health = 1000, int damage = 1)
        {
            Name = name;
            Description = desc;
            Health = health;
            Damage = damage;

            AllEntitys.Add(this);

            ID = IDCount;

            IDCount++;
        }

        //Generally used for actors only otherwise manual entry into allentitys is required
        public Entity()
        {
            //Below command excluded so that Actors have to be called explicitly
            //AllEntitys.Add(this);
        }

        public string Name;
        public string Description;

        public int Health;
        public int Damage;

        public int ID = 0;

        public Entity Location;
        public Entity Owner;

        //Properties of the entity, like AI for example or User controller
        public List<Entity> Actors = new List<Entity>();

        //Things the entity holds, like a chest might hold a gun or a shoe
        public List<Item> Items = new List<Item>();

        public Item SelectedItem = null;

        //Connecting entities that have a relation to this one
        //Example might be a room that connects to another room
        public List<Entity> Connectors = new List<Entity>();

        //Attack another intety
        public void Attack_Entity(Entity Defender)
        {

            //Attackers dice
            int dice1 = R.Next(1, 8);

            //Defenders Dice
            int Ddice = R.Next(1, 8);

            int DefenderPoints = 0;
            int AttackersPoints = 0;

            if (dice1 > Ddice)
            {
                AttackersPoints++;
            }
            else
            {
                DefenderPoints++;
            }


            this.Health -= DefenderPoints * Defender.Damage;
            Defender.Health -= AttackersPoints * this.Damage;

            if (Defender.Health < 0)
            {
                Defender.Health = 0;
            }

            if (this.Health < 0)
            {
                this.Health = 0;
            }

            if (this.SelectedItem != null && this.SelectedItem.Health >= 1)
            {
                //Take away weapons health
                this.SelectedItem.Health -= 1;
            }


        }

        //Static Update for all entities created
        public static void UpdateAll()
        {
            TurnNumber++;
            foreach (Entity E in AllEntitys)
            {
                E.Update();
            }
        }

        //Update for individual entity
        public virtual void Update()
        {
            foreach (Entity A in Actors)
            {
                A.Update();
            }
        }


    }
}
