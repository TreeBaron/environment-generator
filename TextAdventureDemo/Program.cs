﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventureDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //generates domestic items
            Domestic domestic = new Domestic();

            //Deal with player
            Entity Player = new Entity("Player", "This is you, but what am I?", 100, 1);

            //Give player starting item
            Item Fists = new Item("Your fists", "Your hands rolled up into fists.", 7, 2);
            Player.Items.Add(Fists);

            //Create testing rooms
            Entity LivingRoom = new Entity("Living Room", "The living room of your house.", 10000, 2);
            Item Couch = new Item("Sofa", "A blue sofa.", 200, 0);
            Item Lamp = new Item("Lamp", "A very generic lamp, complete with shade.", 10, 1);
            LivingRoom.Items.Add(Couch);
            LivingRoom.Items.Add(Lamp);


            Entity Kitchen = new Entity("Kitchen", "The kitchen of your house.", 20000, 4);
            Item Blender = new Item("Blender", "An off brand blender.", 20, 15);
            Item Dog = new Item("Max", "Your dog max.", 85, 20);
            Item Fridge = new Item("Fridge", "Your fridge.", 400, 0);
            Item OldMustard = new Item("Old Mustard", "Definetly expired, it's purple...", 2, 1);
            Fridge.Items.Add(OldMustard);
            Kitchen.Items.Add(Blender);
            Kitchen.Items.Add(Dog);
            Kitchen.Items.Add(Fridge);

            //Add connectors last
            Kitchen.Connectors.Add(LivingRoom);
            LivingRoom.Connectors.Add(Kitchen);
            Entity bedroom = domestic.GenerateOnePersonBedroom();
            LivingRoom.Connectors.Add(bedroom);

            //Deal with player some more
            Player.Location = LivingRoom;
            PlayerController PC = new PlayerController();
            PC.Owner = Player;
            Player.Actors.Add(PC);




            while (true)
            {
                Entity.UpdateAll();
            }


        }
    }
}
