﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventureDemo
{
    class PlayerController : Entity
    {

        

        public override void Update()
        {
            //Performs default update on all actors within player
            base.Update();

            //If player has no items, they will equip their fists
            if (this.Owner.Items.Count < 1)
            {
                Item Fists = new Item("Your fists", "Your hands rolled up into fists.", 7, 2);
                this.Owner.SelectedItem = Fists;
                this.Owner.Items.Add(Fists);
            }

            bool EndOfTurn = false;

            while (EndOfTurn == false)
            {
                //Tell player what turn it is, where they are, what items are in room
                DisplayLocationInfo();
                string Input = Console.ReadLine();

                //Check if player wants to move to another location
                EndOfTurn = MoveTo(Input);

                //Check if plyaer wants to select an item
                EndOfTurn = SelectItem(Input);

                //Check if player wants to attack something
                EndOfTurn = Attack(Input);

                EndOfTurn = LookAt(Input);

                EndOfTurn = Help(Input);

                EndOfTurn = Take(Input,this.Owner.Location.Items);

                EndOfTurn = DropItem(Input);

                EndOfTurn = Open(Input);

                //Clear line if end of turn still is false and say command not recognized
                if (EndOfTurn == false)
                {
                    Console.WriteLine("\nPress Any Key To Continue...");
                    Console.ReadKey();
                    Console.Clear();
                }
            }

            //Wait for input from player (press any key to continue)
            Console.WriteLine("\nPress Any Key To Continue...");
            Console.ReadKey();
            Console.Clear(); //Clear console after turn has ended


        }

        //Displays the info in the location of what's there and such
        public void DisplayLocationInfo()
        {
            Console.WriteLine("Game Turn Number "+Entity.TurnNumber);

            Console.Write("\n");

            Console.WriteLine(this.Owner.Location.Name);
            Console.WriteLine(this.Owner.Location.Description);

            Console.Write("\n");

            //Say items in room
            SayItemsInLocation();

            Console.Write("\n");

            //Say what connecting places the player can travel to
            SayConnectingLocations();

            
            //Next line!
            Console.WriteLine("\n\n");
            DrawDivider();

            

        }

        public bool Take(string Input, List<Item> SearchList)
        {
            if (ApproximateTextFinder(Input, new List<string> { "take", "retrieve", "grab", "loot", "steal", "pick up" }))
            {
                foreach (Item E in SearchList)
                {
                    if (ApproximateTextFinder(Input, E.Name))
                    {
                        Console.WriteLine("You take " + E.Name);
                        this.Owner.Items.Add(E);
                        SearchList.Remove(E);
                        return false;
                    }
                }
            }

            return false;
        }


        public bool Help(string Input)
        {
            if (ApproximateTextFinder(Input, new List<string> { "help", "commands", "options", "/help", "/commands", "/options"}))
            {
                Console.WriteLine("\nCOMMANDS");
                Console.WriteLine("-----------");
                Console.WriteLine("Selection: type \"select\" to select or equip an item.");
                Console.WriteLine("Combat: type \"attack\" or \"kill\" or \"destroy\" and\n then the items name. e.g. destroy lamp");
                Console.WriteLine("Movement: type \"go\" or \"move\" or \"travel\". e.g. \ntravel to kitchen");
                Console.WriteLine("Awareness: Type \"examine\" or \"look\" or \"inspect\" \nto look at something closely. e.g. look at lamp");
                Console.WriteLine("Inventory Part 1: Type \"take\" or \"steal\" or \"grab\" \nto take an item. e.g. take the lamp");
                Console.WriteLine("Inventory Part 2: Type \"drop\" to drop your equipped item.");

                Console.WriteLine("\nNOTES");
                Console.WriteLine("-----------");
                Console.WriteLine("When you pick up an item it equips automatically. You will be asked before combat which item to use as a weapon.");

            }

            return false;
        }

        //Have player open item container (any item)
        public bool Open(string Input)
        {
            if (ApproximateTextFinder(Input, new List<string> { "open" }))
            {
                foreach (Item E in this.Owner.Location.Items)
                {
                    if (ApproximateTextFinder(Input, E.Name))
                    {
                        //Check if there is a key to item
                        //Check if player has key
                        if (this.Owner.Items.Contains(E.Key) || E.Key == null)
                        {
                            Console.WriteLine("You open " + E.Name);

                            while (true)
                            {
                                //Display item inventory
                                Console.WriteLine("");

                                DisplayItems(E.Items);

                                Console.WriteLine("Type /back to exit container.");
                                string inp = Console.ReadLine();

                                if (inp == "/back")
                                {
                                    break;
                                }
                                else
                                {
                                    Take(inp, E.Items);
                                }

                            }
                        }
                        else
                        {
                            Console.WriteLine(E.Name+" requires a key to open.");
                        }

                    }
                }
            }

            return false;
        }

        //returns bool to indicate whether or not it was handled
        public bool MoveTo(string Input)
        {
            if (ApproximateTextFinder(Input, new List<string> {"move","go to","head","go","tragress","approach","travel","enter"}))
            {
                foreach(Entity E in this.Owner.Location.Connectors)
                {
                    if (ApproximateTextFinder(Input, E.Name))
                    {
                        Entity MoveToLocation = SearchEntity(Input,Owner.Location.Connectors);

                        if (MoveToLocation != null)
                        {
                            //Move to that location then
                            Console.WriteLine("You move from " + Owner.Location.Name + " to "+E.Name+".");
                            Owner.Location = MoveToLocation;
                            return true;
                        }
                                               
                    }
                }
                    
                    Console.WriteLine("I could not find a connecting location called \"" + Input + "\"");
                    return false;
            }

            return false;
        }

        public bool Attack(string Input)
        {
            if (ApproximateTextFinder(Input, new List<string> { "attack", "kill", "beat", "hit", "stab", "damage", "destroy", "murder", "hurt", "wound" }))
            {
                //Find name of thing player wants to attack
                Item ToAttack = SearchEntity(Input,this.Owner.Location.Items);

                Console.WriteLine("Are you sure you want to attack "+ToAttack.Name+"?");
                string temp = Console.ReadLine();

                if (ApproximateTextFinder(temp, new List<string> { "yes", "yep", "yeah", "kill", "attack", "absolutely", "affirmative", "sure", "y" }))
                {

                    //Have player select item!
                    SelectItem();

                    bool AutoComplete = false;

                    while(true)
                    {
                        Console.WriteLine("Your health is "+this.Owner.Health+".");
                        Console.WriteLine(ToAttack.Name+"'s health is "+ToAttack.Health+".");
                        if (this.Owner.SelectedItem != null)
                        {
                            Console.WriteLine("Your weapon's health is " + this.Owner.SelectedItem.Health + ".");
                        }

                        if (AutoComplete == false)
                        {
                            Console.WriteLine("\nContinue attack? Type autocomplete to fight until death.");
                            temp = Console.ReadLine();
                        }
                        else
                        {
                            temp = "yes";
                        }

                        if (ApproximateTextFinder(temp, new List<string> { "yes", "yep", "yeah", "kill", "attack", "absolutely", "affirmative", "sure", "y","continue"}))
                        {
                            this.Owner.Attack_Entity(ToAttack);

                            //If weapon is depleted, destroy it
                            if (this.Owner.SelectedItem != null && this.Owner.SelectedItem.Health <= 0)
                            {
                                this.Owner.Items.Remove(SelectedItem);
                                this.Owner.SelectedItem = null;
                                Console.WriteLine("Your weapon was depleted!");


                                //If the players item breaks, and they have no more items
                                //Should remain after game over check
                                if (this.Owner.Items.Count <= 0)
                                {
                                    Item Fists = new Item("Your fists", "Your hands rolled up into fists.", 7, 2);
                                    this.Owner.SelectedItem = Fists;
                                    this.Owner.Items.Add(Fists);
                                    Console.WriteLine("You're fighting without a weapon now!\n");
                                }
                            }


                            if (ToAttack.Health <= 0)
                            {
                                DrawDivider();
                                Console.WriteLine("You destroy "+ToAttack.Name);
                                DrawDivider();

                                this.Owner.Location.Items.Remove(ToAttack);
                                break;
                            }
                            else if (this.Owner.Health <= 0)
                            {
                                DrawDivider();
                                Console.WriteLine(ToAttack.Name+" HAS DESTROYED YOU!");
                                Console.WriteLine("GAME OVER");
                                DrawDivider();
                                Console.ReadLine();
                                Environment.Exit(0);
                            }



                        }
                        else if (ApproximateTextFinder(temp,"autocomplete"))
                        {
                            AutoComplete = true;
                        }
                        else
                        {
                            break;
                        }
                

                    }

                }
                else
                {
                    return false;
                }

            }
                        
            return false;
        }

        public bool DropItem(string Input)
        {
            if (ApproximateTextFinder(Input, new List<string> { "drop", "trash", "dispose" }))
            {
                //Find name of thing player wants to drop
                Item ToDrop = SearchEntity(Input, this.Owner.Items);

                if (ToDrop != null)
                {
                    Console.WriteLine("You drop " + ToDrop.Name);

                    this.Owner.Items.Remove(ToDrop);
                    this.Owner.Location.Items.Add(ToDrop);

                    if (this.Owner.SelectedItem == ToDrop)
                    {
                        this.Owner.SelectedItem = null;
                    }
                }
                else
                {
                    Console.WriteLine("I'm not sure what you want to drop.");
                }


            }
            return false;
        }

        //Select item as forced to
        public bool SelectItem(bool clearscreen = true)
        {
            if (this.Owner.Items.Count >= 1)
            {
                if (clearscreen == true)
                {
                    Console.Clear();
                }


                Console.WriteLine("These are your items:");

                foreach (Entity E in this.Owner.Items)
                {
                    Console.WriteLine("Name - " + E.Name + " Damage - " + E.Damage + " Health - " + E.Health);
                }

                while (true)
                {
                    Console.WriteLine("Type the name of the item you wish to select!");
                    string Input = Console.ReadLine();

                    if (ApproximateTextFinder(Input, "/back") != true)
                    {
                        foreach (Item E in this.Owner.Items)
                        {
                            if (ApproximateTextFinder(Input, E.Name) == true)
                            {
                                Console.WriteLine(E.Name + " has been equipped!");
                                this.Owner.SelectedItem = E;
                                return false;
                            }
                        }
                    }
                    else
                    { 
                        return false;
                    }

                    Console.WriteLine("I could not find \"" + Input + "\", please enter a valid item or type /back");

                }
            }
            return false;
        }

        //Select item as part of turn
        public bool SelectItem(string Input)
        {
            if (ApproximateTextFinder(Input, new List<string> { "equip", "select item", "choose item", "choose weapon", "select weapon", "equip my","select an item"}))
            {
                //display items

                while (true)
                {
                    Console.WriteLine("Type the name of the item you wish to equip!");
                    string input = Console.ReadLine();

                    if (ApproximateTextFinder(input, "/back") != true)
                    {
                        foreach (Item E in this.Owner.Items)
                        {
                            if (ApproximateTextFinder(input, E.Name) == true)
                            {
                                Console.WriteLine(E.Name + " has been equipped!");
                                this.Owner.SelectedItem = E;
                                return false;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }

                    Console.WriteLine("I could not find \"" + input + "\", please enter a valid item or type /back");

                }
            }

            return false;
        }

        public void DisplayItems(List<Item> List)
        {
            Console.WriteLine("Items:");

            foreach (Entity E in List)
            {
                Console.WriteLine("Name - " + E.Name + " Damage - " + E.Damage + " Health - " + E.Health);
            }
        }

        public void DisplayItems(List<Entity> List)
        {

            Console.WriteLine("Items:");

            foreach (Entity E in List)
            {
                Console.WriteLine("Name - " + E.Name + " Damage - " + E.Damage + " Health - " + E.Health);
            }
        }


        //returns bool to indicate whether or not it was handled
        public bool LookAt(string Input)
        {
            if (ApproximateTextFinder(Input, new List<string> {"examine","look","look at","observe","inspect","stare at"}))
            {
                foreach(Entity E in this.Owner.Location.Items)
                {
                    if (ApproximateTextFinder(Input,E.Name))
                    {
                        Console.WriteLine("You look at "+E.Name+" - "+E.Description);
                        return false;
                    }
                }
            }
            
                return false;
        }

        //Search for entity in list with approximate name, and return it
        public Entity SearchEntity(string Input, List<Entity> Entities)
        {
            //Search location first
            foreach(Entity E in Entities)
            {
                if (ApproximateTextFinder(E.Name,Input))
                {
                    return E;
                }
            }

            return null;
        }

        //Search for entity in list with approximate name, and return it
        //Overloaded for items aswell
        public Item SearchEntity(string Input, List<Item> Entities)
        {
            //Search location first
            foreach (Item E in Entities)
            {
                if (ApproximateTextFinder(E.Name, Input))
                {
                    return E;
                }
            }

            return null;
        }

        //Give it key words and it'll give you AIDS I mean, a boolean value indicating if it found approximations of those words
        public bool ApproximateTextFinder(string Input, List<string> Words)
        {

            foreach(string word in Words)
            {
                if (Input.ToLower() == word.ToLower() || Input.ToLower().Contains(word.ToLower())||word.ToLower().Contains(Input.ToLower()))
                {
                        return true;
                    
                }
            }

            return false;
        }

        //OVERLOAD IMMINENT!!!!! O_- PHASE THE TRANSDUCTORS THROUGH THE BIPASS NETWORK1!!11!!
        public bool ApproximateTextFinder(string Input, string Word)
        {
                if (Input.ToLower() == Word.ToLower() || Input.ToLower().Contains(Word.ToLower()) || Word.ToLower().Contains(Input.ToLower()))
                {
                        return true;
                }
         
            return false;
        }

        //Say all the places the player can travel
        public void SayConnectingLocations()
        {
            //Say where this place connects to if it connects anywhere :P
            if (this.Owner.Location.Connectors.Count >= 1)
            {
                Console.Write("\n\nTravel Locations:");
                for (int i = 0; i < Owner.Location.Connectors.Count; i++)
                {
                    if (i != 0)
                    {
                        Console.Write(", " + Owner.Location.Connectors[i].Name);
                    }
                    else
                    {
                        Console.Write(" " + Owner.Location.Connectors[i].Name);
                    }
                }
            }
        }
        
        //Say all the items the player can interact with in location
        public void SayItemsInLocation()
        {
            if (this.Owner.Location.Items.Count > 1)
            {
                Console.WriteLine("In this place you see ");
                //Loop through other "items" in location
                for (int i = 0; i < this.Owner.Location.Items.Count; i++)
                {
                    if (i < this.Owner.Location.Items.Count - 1)
                    {
                        Console.Write(this.Owner.Location.Items[i].Name + ", ");
                    }
                    else
                    {
                        Console.Write("and " + this.Owner.Location.Items[i].Name + ".");
                    }
                }
            }
            else if (this.Owner.Location.Items.Count == 1)
            {
                Console.Write("In this place you see ");
                Console.Write(this.Owner.Location.Items[0].Name);
            }
            //No items in location
            else
            {
                Console.WriteLine("You see nothing of note here.");
            }
        }

        //Writes a line of -------------
        public void DrawDivider()
        {
            Console.WriteLine("------------------------------");
        }
    }
}
